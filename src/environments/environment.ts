// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyBqD39LtnCK3PqxyE6gne4sC_G8PPcSK8w",
    authDomain: "controle-gu3002284.firebaseapp.com",
    databaseURL: "https://controle-gu3002284.firebaseio.com",
    projectId: "controle-gu3002284",
    storageBucket: "controle-gu3002284.appspot.com",
    messagingSenderId: "861820882230",
    appId: "1:861820882230:web:a2cbcad3233ebc8f67f797"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
