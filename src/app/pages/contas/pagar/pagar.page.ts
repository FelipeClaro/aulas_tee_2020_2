import { Component, OnInit } from '@angular/core';
import { DocumentData } from '@angular/fire/firestore';
import { ContaService } from '../service/conta-service';

@Component({
  selector: 'app-pagar',
  templateUrl: './pagar.page.html',
  styleUrls: ['./pagar.page.scss'],
})
export class PagarPage implements OnInit {
  contas: DocumentData[];
  constructor(
    private contaService: ContaService
  ) { }

  ngOnInit() {
    this.carregarContas();
  }

  carregarContas() {
    this.contaService.listar('pagar')
      .subscribe(listaContas => this.contas = listaContas)
  }

  removerConta(conta: any) {
    this.contaService.remove(conta);
  }
}
