import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavController } from '@ionic/angular';
import { ContaService } from '../service/conta-service';

@Component({
  selector: 'app-cadastro',
  templateUrl: './cadastro.page.html',
  styleUrls: ['./cadastro.page.scss'],
})
export class CadastroPage implements OnInit {
  contasForm: FormGroup;

  constructor(
    private builder: FormBuilder,
    private nav: NavController,
    private contaService: ContaService
  ) { }

  ngOnInit() {
    this.initForm();
  }

  private initForm() {
    this.contasForm = this.builder.group({
      tipo: ['', Validators.required],
      valor: ['', [Validators.required, Validators.min(0.01)]],
      parceiroComercial: ['', [Validators.required, Validators.minLength(5)]],
      descricao: ['', [Validators.required, Validators.minLength(6)]],
    });
  }

  registraConta() {
    this.contaService.registrar(this.contasForm.value).then(() => {
      this.nav.navigateForward('contas/pagar')
    });
  }

}
