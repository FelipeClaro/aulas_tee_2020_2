import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AuthRoutingModule } from './auth-routing.module';
import { TopoLoginComponent } from './component/topo-login/topo-login.component';


@NgModule({
  declarations: [],
  imports: [
    AuthRoutingModule,
    CommonModule
  ]
})
export class AuthModule { }
