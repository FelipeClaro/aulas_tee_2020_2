import { Component, OnInit } from '@angular/core';
import { NavController, ToastController } from '@ionic/angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoginService } from '../service/login.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  loginForm: FormGroup;

  constructor(
    private builder: FormBuilder,
    private service: LoginService,
    private nav: NavController) {
  }

  ngOnInit() {
    this.isUserLoggedIn();

    this.loginForm = this.builder.group({
      email: ["", [Validators.email, Validators.required]],
      password: ["", [Validators.required, Validators.minLength(8)]]
    });
  }

  isUserLoggedIn() {
    this.service.isLoogedIn.subscribe(user => {
      if (user) {
        this.nav.navigateForward("home");
      }
    });
  }

  login() {

    const user = this.loginForm.value;
    console.log(user);
    this.service.login(user);
  }
}
