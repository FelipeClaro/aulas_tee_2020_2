import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { NavController, ToastController } from '@ionic/angular';
import { User } from 'firebase';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  isLoogedIn: Observable<User>;

  constructor(
    private nav: NavController,
    private toast: ToastController,
    private auth: AngularFireAuth) {
    this.isLoogedIn = auth.authState;
  }

  login(user) {
    this.auth.signInWithEmailAndPassword(user.email, user.password)
      .then(() => {
        this.nav.navigateForward("home");
      })
      .catch(() => {
        this.showError();
      });
  }

  register(user) {
    this.auth.createUserWithEmailAndPassword(user.email, user.password);
  }

  recoverPassword(email) {
    this.auth.sendPasswordResetEmail(email).then(() => {
      this.nav.navigateBack("auth");
    }).catch(error => {
      console.log(error);

    });
  }

  logout() {
    this.auth.signOut().then(() => {
      this.nav.navigateBack("auth");
    });
  }

  private async showError() {
    const errorToast = await this.toast.create({
      message: "Dados de acesso incorretos",
      duration: 3000
    });

    errorToast.present();
  }
}
